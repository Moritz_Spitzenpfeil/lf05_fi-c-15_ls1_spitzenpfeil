import java.util.Scanner;

class Aufgabe_3_3
{
    public static void main(String[] args) {
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
       double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(r�ckgabebetrag);   
    } 
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double zuZahlenderBetrag;
    	int ticketanzahl;
    	
    	System.out.print("Ticketpreis (Euro): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        
        //Ticketanzahl
        // -----------
        
        System.out.println("Anzahl der Tickets: ");
        ticketanzahl = tastatur.nextInt();
        return zuZahlenderBetrag *= ticketanzahl;
    }
    
    public static double fahrkartenBezahlen (double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag;
        double eingeworfeneM�nze;
        double r�ckgabebetrag;
        
        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
     	   System.out.print("Eingabe (mindestens 5 Cent, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        tastatur.close();
        return r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben (double r�ckgabebetrag) {
    	
    	// R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        if(r�ckgabebetrag > 0.0) {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro\n", r�ckgabebetrag);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 Euro-M�nzen
            {
         	  System.out.println("2 Euro");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 Euro-M�nzen
            {
         	  System.out.println("1 Euro");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 Cent-M�nzen
            {
         	  System.out.println("50 Cent");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 Cent-M�nzen
            {
         	  System.out.println("20 Cent");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 Cent-M�nzen
            {
         	  System.out.println("10 Cent");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 Cent-M�nzen
            {
         	  System.out.println("5 Cent");
  	          r�ckgabebetrag -= 0.05;
            }
        }
        
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
    }
   
}

//Es wurde der primitive Datentyp double verwendet, da dieser Datentyp besonders genaue Gleitkommazahlen
//darstellen kann, was zu genaueren Berechnungen, Rundungen und korrekten Ergebnissen f�hrt.
//Bei der Berechnung der Ticketpreise wird der Nutzer zun�chst gebeten, die Menge des zu bezahlenden Preises pro einzelnem Ticket einzugeben.
//Dieser Wert wird der Variable zuZahlenderBetrag zugewiesen.
//Anschlie�end wird der Nutzer gebeten die Anzahl an gekauften Tickets einzugeben, dieser Wert wird der Variable ticketanzahl zugewiesen.
//Diese beiden Werte nimmt das Programm �ber die Scanner-Funktion und mutipliziert diese miteinander.
//Das Ergebnis wird der Variable zuZahlenderBetrag neu zugewiesen.
