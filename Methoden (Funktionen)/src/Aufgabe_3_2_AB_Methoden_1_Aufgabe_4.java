import java.util.Scanner;
public class Aufgabe_3_2_AB_Methoden_1_Aufgabe_4 {
	
		public static void main(String[] args) {
			
			Scanner scan = new Scanner(System.in);
			System.out.println("Geben Sie die L�nge/Breite/H�he des W�rfels ein: ");
			double a1 = scan.nextDouble();
			System.out.printf("Das Volumen des W�rfels betr�gt = %.2f\n", w�rfel(a1));
			System.out.println("Geben Sie die L�nge des Quaders ein: ");
			double a2 = scan.nextDouble();
			System.out.println("Geben Sie die Breite des Quaders ein: ");
			double b = scan.nextDouble();
			System.out.println("Geben Sie die H�he des Quaders ein: ");
			double c = scan.nextDouble();
			System.out.printf("Das Volumen des Quaders betr�gt = %.2f\n", quader(a2, b, c));
			System.out.println("Geben Sie die Breite/L�nge der Pyramide ein: ");
			double a3 = scan.nextDouble();
			System.out.println("Geben Sie die H�he der Pyramide ein: ");
			double h = scan.nextDouble();
			System.out.printf("Das Volumen der Pyramide betr�gt = %.2f\n", pyramide(a3, h));
			System.out.println("Geben Sie den Radius der Kugel ein: ");
			double r = scan.nextDouble();
			System.out.printf("Das Volumen der Kugel betr�gt = %.2f\n", kugel(r));
			scan.close();
			
		}
		
		public static double w�rfel(double a1) {
			return a1 * a1 * a1;
		}
		public static double quader(double a2, double b, double c) {
			return a2 * b * c;
		}
		public static double pyramide(double a3, double h) {
			return (double)a3 * (double)a3 * (double)h / (double)3;
		}
		public static double kugel(double r) {
			final double pi = 3.1415;
			return (double)4/(double)3 * (r * r * r) * pi;
		}
		
	}

