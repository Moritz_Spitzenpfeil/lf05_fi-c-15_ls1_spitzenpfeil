import java.util.Scanner;
public class Aufgabe_3_1_Mittelwert {

   public static void main(String[] args) {
	  
	  

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
	   Scanner scan = new Scanner(System.in);
	   System.out.println("Geben Sie eine Zahl ein: ");
	   double x = scan.nextDouble();
	   System.out.println("Geben Sie eine Zahl ein: ");
	   double y = scan.nextDouble();
	   double m;
	   scan.close();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = mittelwert(x, y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      ausgabe(x, y, m);
   }
   public static double mittelwert(double x, double y) {
	   return (x + y) / 2.0;
   }
   
   public static void ausgabe(double x, double y, double m) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
}

