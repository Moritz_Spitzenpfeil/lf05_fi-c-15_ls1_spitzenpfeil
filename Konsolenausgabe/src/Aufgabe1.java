
public class Aufgabe1 {

	public static void main(String[] args) {
		
		System.out.print("Das ist ein Beispielsatz.");
		System.out.println(" Ein Beispielsatz ist das.");
		System.out.print("Das ist ein \"Beispielsatz\" mit Escape-Symbolen.\n");
		String str1 = "Beispielsatz", str2 = "das";
		System.out.println("Ein " + str1 + " ist " +
		str2 + ".");
		
		// Die print()-Anweisung gibt einen eingegebenen String aus, die println()-Anweisung gibt einen String aus und f�gt automatisch einen Zeilenumbruch dahinter ein.

	}

}
