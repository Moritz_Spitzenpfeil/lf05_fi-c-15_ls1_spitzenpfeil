import java.util.Scanner;
public class Aufgabe_4_1_AB_Auswahlstrukturen_Aufgabe_1 {
	
		public static void main(String[] args) {
			
			// Wenn-Dann-Aktivit�ten aus dem Alltag:
			// Wenn das Handy sich am Standort "zuhause" befindet, aktiviert es das WLAN und schaltet die mobilen Daten ab.
			// Wenn der Hund den Nachbarn sieht, bellt er laut.
			// Wenn Wochenende ist, kann entspannt werden.
			Scanner scanner1 = new Scanner(System.in);
			System.out.println("Bitte geben Sie die erste Zahl ein: ");
			int x = scanner1.nextInt();
			System.out.println("Bitte geben Sie die zweite Zahl ein: ");
			int y = scanner1.nextInt();
			ueberpruefung1(x, y);
			ueberpruefung2(x, y);
			System.out.println("Bitte geben Sie die erste Zahl ein: ");
			x = scanner1.nextInt();
			System.out.println("Bitte geben Sie die zweite Zahl ein: ");
			y = scanner1.nextInt();
			System.out.println("Bitte geben Sie die dritte Zahl ein: ");
			int z = scanner1.nextInt();
			ueberpruefung3(x, y, z);
			ueberpruefung4(x, y, z);
			scanner1.close();
		}
		
		public static void ueberpruefung1(int x, int y) {
			if (x == y)
			{
			System.out.println("Zahl 1 ist gleich wie Zahl 2.");
			}
			else if (x != y)
			System.out.println("Zahl 1 ist ungleich Zahl 2!");
		}
				
		public static void ueberpruefung2(int x, int y) {
			if (x < y)
			{
			System.out.println("Zahl 1 ist kleiner als Zahl 2.");	
			}
			else if (x >= y)
			{
			System.out.println("Zahl 1 ist gr��er als Zahl 2 oder gleich wie Zahl 2.");	
			}
			else
			{
			System.out.println("Es ist ein Fehler aufgetreten, bitte versuchen Sie es erneut!");	
			}
		}
		
		public static void ueberpruefung3(int x, int y, int z) {
			if (x > y && x > z)
			{
			System.out.println("Zahl 1 ist gr��er als Zahl 2 und gr��er als Zahl 3.");
			}
			else if (z > y || z > x)
			System.out.println("Zahl 3 ist gr��er als Zahl 1 oder gr��er als Zahl 2.");
		}
		
		public static void ueberpruefung4(int x, int y, int z) {
			if (x == y && x == z)
			{
			System.out.println("Alle drei Zahlen sind gleich gro�.");
			}
			else if (x > y && x > z)
			{
			System.out.println("Zahl 1 ist die gr��te Zahl: " + x);
			}
			else if (y > x && y > z)
			{
			System.out.println("Zahl 2 ist die gr��te Zahl: " + y);
			}
			else if (z > x && z > y)
			{
			System.out.println("Zahl 3 ist die gr��te Zahl: " + z);
			}
		}
}