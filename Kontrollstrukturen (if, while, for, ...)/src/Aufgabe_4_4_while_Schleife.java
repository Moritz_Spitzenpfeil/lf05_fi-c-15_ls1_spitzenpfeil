import java.util.Scanner;

public class Aufgabe_4_4_while_Schleife {

	public static void main(String[] args) {
		
		int x;
		int i;

		Scanner scanner1 = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Zahl n ein: ");
		x = scanner1.nextInt();
		scanner1.close();

		System.out.println("a) heraufz�hlend");
		i = 0;
		while (i <= x) {
			System.out.println(i);
			i++;
		}
		System.out.println("b) herunterz�hlend");
		i = x;
		while (i >= 0) {
			System.out.println(i);
			i--;
		}

	}
}
