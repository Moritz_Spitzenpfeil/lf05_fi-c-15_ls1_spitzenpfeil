import java.util.Scanner;

public class Aufgabe_4_4_do_while_Schleife {

	public static void main(String[] args) {

		int x;
		int i;

		Scanner scanner1 = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Zahl n ein: ");
		x = scanner1.nextInt();
		scanner1.close();

		System.out.println("a) heraufz�hlend");
		i = 0;
		do {
			System.out.println(i);
			i++;
		} while (i <= x);

		System.out.println("b) herabz�hlend");
		i = x;
		do {
			System.out.println(i);
			i--;
		} while (i >= 0);

	}
}