import java.util.Scanner;

public class Aufgabe_4_4_for_Schleife {

	public static void main(String[] args) {
		
		int x;
		int i;
		
		Scanner scanner1 = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Zahl n ein: ");
		x = scanner1.nextInt();
		scanner1.close();

		for (i = 0; i < x; i++) {
			System.out.println(i);
		}

	}
}