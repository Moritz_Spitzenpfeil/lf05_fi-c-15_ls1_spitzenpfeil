
import java.util.Scanner;
public class Aufgabe_2_2_AB_Konsolenausgabe_Aufgabe_2 {

	public static void main(String[] args) {
		
		System.out.println("Hallo!");
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Wie hei�t Du?");
		String name = scan.next();
		
		System.out.println("Wie alt bist Du?");
		int age = scan.nextInt();
		
		scan.close();
		
		System.out.print("Dein Name lautet " + name + " und Du bist " + age + " Jahre alt.");
	}

}
