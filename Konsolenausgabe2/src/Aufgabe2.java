
public class Aufgabe2 {

	public static void main(String[] args) {
		
		String a = "1 * 2 * 3 * 4 * 5";
		System.out.printf("%-5s%s%-19.0s%s%4s\n", "0!", "= ", a, "=", 1);
		System.out.printf("%-5s%s%-19.1s%s%4s\n", "1!", "= ", a, "=", 1);
		System.out.printf("%-5s%s%-19.5s%s%4s\n", "2!", "= ", a, "=", 2);
		System.out.printf("%-5s%s%-19.9s%s%4s\n", "3!", "= ", a, "=", 6);
		System.out.printf("%-5s%s%-19.13s%s%4s\n", "4!", "= ", a, "=", 24);
		System.out.printf("%-5s%s%-19s%s%4s", "5!", "= ", a, "=", 120);

	}

}
